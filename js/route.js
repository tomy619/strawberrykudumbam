	// create the module and name it scotchApp
	var scotchApp = angular.module('scotchApp', ['ngRoute']);

	// configure our routes
	scotchApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'partials/recipelist.html',
				controller  : 'mainController'
			})

			// route for the about page
			.when('/all', {
				templateUrl : 'partials/all.html',
				controller  : 'mainController'
			})
            
            .when('/chicken', {
				templateUrl : 'partials/recipename.html',
				controller  : 'mainController'
			})
        
        .when('/fish', {
				templateUrl : 'partials/fishrecipe.html',
				controller  : 'mainController'
			})
        
         .when('/beef', {
				templateUrl : 'partials/beefrecipe.html',
				controller  : 'mainController'
			})
        
         .when('/mutton', {
				templateUrl : 'partials/muttonrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/ramzan', {
				templateUrl : 'partials/ramzanrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/onam', {
				templateUrl : 'partials/onamrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/christmas', {
				templateUrl : 'partials/christmasrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/veg', {
				templateUrl : 'partials/vegrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/pickle', {
				templateUrl : 'partials/picklerecipe.html',
				controller  : 'mainController'
			})
            
            .when('/breakfast', {
				templateUrl : 'partials/breakfastrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/nadan', {
				templateUrl : 'partials/nadanrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/palaharam', {
				templateUrl : 'partials/palaharangal.html',
				controller  : 'mainController'
			})
            
            .when('/cake', {
				templateUrl : 'partials/cakerecipe.html',
				controller  : 'mainController'
			})
            
            .when('/dessert', {
				templateUrl : 'partials/dessertrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/baked', {
				templateUrl : 'partials/bakedrecipe.html',
				controller  : 'mainController'
			})
            
            .when('/thattukada', {
				templateUrl : 'partials/thattukadarecipe.html',
				controller  : 'mainController'
			})
            
            
            .when('/biriyani', {
				templateUrl : 'partials/biriyanirecipe.html',
				controller  : 'mainController'
			})
        
        
            .when('/kitchen', {
				templateUrl : 'partials/kitchentips.html',
				controller  : 'mainController'
			})
        
        
        
            .when('/chicken/:bId', {
                templateUrl : 'partials/recipedetail.html',
                controller : 'mainController'
            }) 

			// route for the contact page
			.when('/contact', {
				templateUrl : 'pages/contact.html',
				controller  : 'contactController'
			});
	});

	// create the controller and inject Angular's $scope
	scotchApp.controller('mainController', function($scope, $http, $routeParams)  {

	$http.get('https://tomym619.cloudant.com/strawberrykudumbam/_design/strawberryView/_view/all').success(function(data) {
        $scope.onam = data;
         $scope.whichItem1 = $routeParams.bId;
	});
        
    $http.get('https://tomym619.cloudant.com/strawberrykudumbam/_design/strawberryView/_view/all').success(function(data1) {
    $scope.slide = data1;

});
    $http.get('https://tomym619.cloudant.com/strawberrykudumbamlatestnews/_design/newsview/_view/news').success(function(news) {
    $scope.news1 = news;

});
   
    

   
      
});


	scotchApp.controller('aboutController', function($scope) {
		$scope.message = 'Look! I am an about page.';
	});

	scotchApp.controller('contactController', function($scope) {
		$scope.message = 'Contact us! JK. This is just a demo.';
	});